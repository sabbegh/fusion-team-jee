package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.CustomizedAds;

@Remote
public interface CustomizedAdsServicesRemote {

	Boolean addCustomizedAds(CustomizedAds theCustomizedAds);

	Long addCustomizedAdsAndReturnId(CustomizedAds theCustomizedAds);

	CustomizedAds findCustomizedAdsById(Long theId);

	Boolean updateCustomizedAds(CustomizedAds theCustomizedAds);

	Boolean deleteCustomizedAds(CustomizedAds theCustomizedAds);

	Boolean deleteCustomizedAdsById(Long theId);

	List<CustomizedAds> findAllCustomizedAds();

}
