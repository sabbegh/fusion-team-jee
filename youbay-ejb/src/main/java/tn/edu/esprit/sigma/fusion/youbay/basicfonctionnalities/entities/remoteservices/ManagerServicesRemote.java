package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Manager;

@Remote
public interface ManagerServicesRemote {

	Boolean addManager(Manager theManager);

	Long addManagerAndReturnId(Manager theManager);

	Manager findManagerById(Long theId);

	Boolean updateManager(Manager theManager);

	Boolean deleteManager(Manager theManager);

	Boolean deleteManagerById(Long theId);

	List<Manager> findAllManager();

}
