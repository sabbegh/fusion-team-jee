package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReviewId;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.OrderAndReviewServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.OrderAndReviewServicesRemote;

/**
 * Session Bean implementation class OrderAndReviewServices
 */
@Stateless
public class OrderAndReviewServices implements OrderAndReviewServicesRemote,
		OrderAndReviewServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public OrderAndReviewServices() {
	}

	@Override
	public Boolean addOrderAndReview(OrderAndReview theOrderAndReview) {
		Boolean b = false;
		try {
			entityManager.persist(theOrderAndReview);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public OrderAndReview findOrderAndReviewById(
			OrderAndReviewId theOrderAndReviewIdClass) {
		return entityManager.find(OrderAndReview.class,
				theOrderAndReviewIdClass);
	}

	@Override
	public Boolean updateOrderAndReview(OrderAndReview theOrderAndReview) {
		boolean b = false;
		try {
			entityManager.merge(theOrderAndReview);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteOrderAndReviewById(
			OrderAndReviewId theOrderAndReviewIdClass) {
		// ATTENTION!
		// this is a composed key, we can't deleted unless
		// we delete the key-classes
		Boolean b = false;
		try {
			OrderAndReview o = findOrderAndReviewById(theOrderAndReviewIdClass);
			entityManager.remove(o);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrderAndReview> findAllOrderAndReview() {
		Query query = entityManager
				.createQuery("select e from OrderAndReview e ");
		return query.getResultList();
	}

}
