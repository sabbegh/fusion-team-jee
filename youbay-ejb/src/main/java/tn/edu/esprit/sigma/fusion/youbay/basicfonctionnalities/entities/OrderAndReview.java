package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: OrderAndReview
 *
 */
@Entity
@Table(name = "T_ORDERANDREVIEW")
public class OrderAndReview implements Serializable {

	/* Embedded ID */
	private OrderAndReviewId orderAndReviewId;

	/* Link Attributes */

	private Buyer buyer;
	private Product product;

	/* "Regular" Attributes */

	/* =======> Order Part <======= */
	private Float pricePaidByBuyer;
	private String initialMessageToSeller;
	private Boolean oderFulfilledBySeller;
	private Boolean hasFiledComplaint;
	private Boolean orderDeliveredToBuyer;

	/* =======> Review Part <======= */
	private Boolean buyerHasLeftAReview;
	private String reviewTitle;
	private String reviewText;
	private Integer productRating;

	/* "Virtual" attribute */
	/* private Date orderingDate; */

	private static final long serialVersionUID = 1L;

	public OrderAndReview() {

	}

	public OrderAndReview(Float pricePaidByBuyer,
			String initialMessageToSeller, Boolean oderFulfilledBySeller,
			Boolean hasFiledComplaint, Date orderDate,
			Boolean orderDeliveredToBuyer, Boolean buyerHasLeftAReview,
			String reviewTitle, String reviewText, Integer productRating,
			Buyer buyer, Product product) {
		super();
		this.orderAndReviewId = new OrderAndReviewId(product.getProductId(),
				buyer.getYouBayUserId(), orderDate);
		this.pricePaidByBuyer = pricePaidByBuyer;
		this.initialMessageToSeller = initialMessageToSeller;
		this.oderFulfilledBySeller = oderFulfilledBySeller;
		this.hasFiledComplaint = hasFiledComplaint;
		this.orderDeliveredToBuyer = orderDeliveredToBuyer;
		this.buyerHasLeftAReview = buyerHasLeftAReview;
		this.reviewTitle = reviewTitle;
		this.reviewText = reviewText;
		this.productRating = productRating;
		this.buyer = buyer;
		this.product = product;
	}

	@EmbeddedId
	public OrderAndReviewId getOrderAndReviewId() {
		return orderAndReviewId;
	}

	public void setOrderAndReviewId(OrderAndReviewId orderAndReviewId) {
		this.orderAndReviewId = orderAndReviewId;
	}

	public Float getPricePaidByBuyer() {
		return pricePaidByBuyer;
	}

	public void setPricePaidByBuyer(Float pricePaidByBuyer) {
		this.pricePaidByBuyer = pricePaidByBuyer;
	}

	@Column(length = 1000)
	public String getInitialMessageToSeller() {
		return initialMessageToSeller;
	}

	public void setInitialMessageToSeller(String initialMessageToSeller) {
		this.initialMessageToSeller = initialMessageToSeller;
	}

	public Boolean getOderFulfilledBySeller() {
		return oderFulfilledBySeller;
	}

	public void setOderFulfilledBySeller(Boolean oderFulfilledBySeller) {
		this.oderFulfilledBySeller = oderFulfilledBySeller;
	}

	public Boolean getHasFiledComplaint() {
		return hasFiledComplaint;
	}

	public void setHasFiledComplaint(Boolean hasFiledComplaint) {
		this.hasFiledComplaint = hasFiledComplaint;
	}

	public Date virtualGetOrderingDate() {
		return this.orderAndReviewId.getTheDate();
	}

	public void virtualSetOrderingDate(Date orderDate) {
		this.orderAndReviewId.setTheDate(orderDate);
	}

	public Boolean getOrderDeliveredToBuyer() {
		return orderDeliveredToBuyer;
	}

	public void setOrderDeliveredToBuyer(Boolean orderDeliveredToBuyer) {
		this.orderDeliveredToBuyer = orderDeliveredToBuyer;
	}

	public Boolean getBuyerHasLeftAReview() {
		return buyerHasLeftAReview;
	}

	public void setBuyerHasLeftAReview(Boolean buyerHasLeftAReview) {
		this.buyerHasLeftAReview = buyerHasLeftAReview;
	}

	@Column(length = 100)
	public String getReviewTitle() {
		return reviewTitle;
	}

	public void setReviewTitle(String reviewTitle) {
		this.reviewTitle = reviewTitle;
	}

	@Column(length = 1000)
	public String getReviewText() {
		return reviewText;
	}

	public void setReviewText(String reviewText) {
		this.reviewText = reviewText;
	}

	public Integer getProductRating() {
		return productRating;
	}

	public void setProductRating(Integer productRating) {
		this.productRating = productRating;
		/**
		 * 
		 */
	}

	@ManyToOne
	@JoinColumn(name = "BuyerId", referencedColumnName = "youBayUserId", updatable = false, insertable = false)
	public Buyer getBuyer() {
		return buyer;
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = buyer;
	}

	@ManyToOne
	@JoinColumn(name = "ProductId", referencedColumnName = "productId", updatable = false, insertable = false)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	@Override
	public String toString() {
		return "OrderAndReview [orderAndReviewId=" + orderAndReviewId
				+ ", buyer=" + buyer + ", product=" + product
				+ ", pricePaidByBuyer=" + pricePaidByBuyer
				+ ", initialMessageToSeller=" + initialMessageToSeller
				+ ", oderFulfilledBySeller=" + oderFulfilledBySeller
				+ ", hasFiledComplaint=" + hasFiledComplaint
				+ ", orderDeliveredToBuyer=" + orderDeliveredToBuyer
				+ ", buyerHasLeftAReview=" + buyerHasLeftAReview
				+ ", reviewTitle=" + reviewTitle + ", reviewText=" + reviewText
				+ ", productRating=" + productRating + "]";
	}


}