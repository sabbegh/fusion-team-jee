package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;

@Local
public interface SubcategoryServicesLocal {

	Boolean addSubcategory(Subcategory theSubcategory);

	Long addSubcategoryAndReturnId(Subcategory theSubcategory);

	Subcategory findSubcategoryById(Long theId);

	Boolean updateSubcategory(Subcategory theSubcategory);

	Boolean deleteSubcategory(Subcategory theSubcategory);

	Boolean deleteSubcategoryById(Long theId);

	List<Subcategory> findAllSubcategory();

}
