package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.AssistantItems;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.AssistantItemsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.AssistantItemsServicesRemote;

/**
 * Session Bean implementation class AssistantItemsServices
 */
@Stateless
public class AssistantItemsServices implements AssistantItemsServicesRemote,
		AssistantItemsServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public AssistantItemsServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Boolean addAssistantItems(AssistantItems theAssistantItems) {
		Boolean b = false;
		try {
			entityManager.persist(theAssistantItems);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public AssistantItems findAssistantItemsById(Long theId) {
		return entityManager.find(AssistantItems.class, theId);
	}

	@Override
	public Boolean updateAssistantItems(AssistantItems theAssistantItems) {
		boolean b = false;
		try {
			entityManager.merge(theAssistantItems);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteAssistantItems(AssistantItems theAssistantItems) {
		Boolean b = false;
		try {
			theAssistantItems = findAssistantItemsById(theAssistantItems
					.getAssistantItemsId());
			entityManager.remove(theAssistantItems);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteAssistantItemsById(Long theId) {
		Boolean b = false;
		try {
			entityManager.remove(findAssistantItemsById(theId));
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AssistantItems> findAllAssistantItems() {
		Query query = entityManager
				.createQuery("select e from AssistantItems e ");
		return query.getResultList();
	}

	@Override
	public Long addAssistantItemsAndReturnId(AssistantItems theAssistantItems) {

		try {
			entityManager.persist(theAssistantItems);
			entityManager.flush();
			return theAssistantItems.getAssistantItemsId();
		} catch (Exception e) {
		}
		return null;
	}

}
