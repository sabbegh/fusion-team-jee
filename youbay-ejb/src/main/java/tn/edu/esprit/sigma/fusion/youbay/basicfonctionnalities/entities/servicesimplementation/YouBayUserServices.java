package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.YouBayUser;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.YouBayUserServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.YouBayUserServicesRemote;

/**
 * Session Bean implementation class YouBayUserServices
 */
@Stateless
public class YouBayUserServices implements YouBayUserServicesRemote, YouBayUserServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public YouBayUserServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public YouBayUser findYouBayUserByEmail(String email, String password) {

		YouBayUser youBayUser = null;
		Query query = entityManager
				.createQuery("SELECT u FROM YouBayUser u where u.email=:email and u.password=:password");
		query.setParameter("email", email).setParameter("password", password);
		try {
			youBayUser = (YouBayUser) query.getSingleResult();
		} catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).log(Level.WARNING,
					"authentication failed with email=" + email + " and password=" + password);
		}
		return youBayUser;
	}

	@Override
	public YouBayUser findYouBayUserByEmail(String email) {
		YouBayUser youBayUser = null;
		Query query = entityManager.createQuery("SELECT u FROM YouBayUser u where u.email=:email");
		query.setParameter("email", email);
		try {
			youBayUser = (YouBayUser) query.getSingleResult();
		} catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "authentication failed with email=" + email);
		}
		return youBayUser;
	}
}
