/*
 * 
 * Author Marwen
 */
package tn.edu.esprit.sigma.fusion.youbay.processing;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;

@Remote
public interface BuyerBusinessServicesRemote {
	public void BuyProduct(Buyer buyer,Product product);
	public void AddProductToCart(Buyer buyer,Product product);
	public void ConfirmOrder(Buyer buyer,OrderAndReview order);
	public String CompareProducts(Product product1 , Product product2);
	public List<Product> FindProductsAdvanced(Float highestPrice , Float lowestPrice ,Subcategory subcategory ,String subCategoryAttributes);
}
