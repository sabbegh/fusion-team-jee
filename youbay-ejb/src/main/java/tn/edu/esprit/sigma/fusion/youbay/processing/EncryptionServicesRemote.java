/*
 * 
 * Author Marwen
 */
package tn.edu.esprit.sigma.fusion.youbay.processing;

import javax.ejb.Remote;

@Remote
public interface EncryptionServicesRemote {
	public String encryptString(String string);
	public String byteToHex(final byte[] hash);
}
