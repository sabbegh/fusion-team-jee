package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReviewId;

@Remote
public interface OrderAndReviewServicesRemote {

	Boolean addOrderAndReview(OrderAndReview theOrderAndReview);

	OrderAndReview findOrderAndReviewById(
			OrderAndReviewId theOrderAndReviewIdClass);

	Boolean updateOrderAndReview(OrderAndReview theOrderAndReview);

	Boolean deleteOrderAndReviewById(OrderAndReviewId theOrderAndReviewIdClass);

	List<OrderAndReview> findAllOrderAndReview();
}