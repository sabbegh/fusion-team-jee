package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViews;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViewsId;

@Local
public interface HistoryOfViewsServicesLocal {

	Boolean addHistoryOfViews(HistoryOfViews theHistoryOfViews);

	HistoryOfViews findHistoryOfViewsById(
			HistoryOfViewsId theHistoryOfViewsIdClass);

	Boolean updateHistoryOfViews(HistoryOfViews theHistoryOfViews);

	Boolean deleteHistoryOfViewsById(HistoryOfViewsId theHistoryOfViewsIdClass);

	List<HistoryOfViews> findAllHistoryOfViews();
}
