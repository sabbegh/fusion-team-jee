package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViews;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViewsId;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.HistoryOfViewsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.HistoryOfViewsServicesRemote;

/**
 * Session Bean implementation class HistoryOfViewsServices
 */
@Stateless
public class HistoryOfViewsServices implements HistoryOfViewsServicesRemote,
		HistoryOfViewsServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public HistoryOfViewsServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Boolean addHistoryOfViews(HistoryOfViews theHistoryOfViews) {
		Boolean b = false;
		try {
			entityManager.persist(theHistoryOfViews);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public HistoryOfViews findHistoryOfViewsById(
			HistoryOfViewsId theHistoryOfViewsIdClass) {
		return entityManager.find(HistoryOfViews.class,
				theHistoryOfViewsIdClass);
	}

	@Override
	public Boolean updateHistoryOfViews(HistoryOfViews theHistoryOfViews) {
		boolean b = false;
		try {
			entityManager.merge(theHistoryOfViews);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteHistoryOfViewsById(
			HistoryOfViewsId theHistoryOfViewsIdClass) {
		// ATTENTION!
		// this is a composed key, we can't deleted unless
		// we delete the key-classes
		Boolean b = false;
		try {
			HistoryOfViews o = findHistoryOfViewsById(theHistoryOfViewsIdClass);
			entityManager.remove(o);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HistoryOfViews> findAllHistoryOfViews() {
		Query query = entityManager
				.createQuery("select e from HistoryOfViews e ");
		return query.getResultList();
	}

}
