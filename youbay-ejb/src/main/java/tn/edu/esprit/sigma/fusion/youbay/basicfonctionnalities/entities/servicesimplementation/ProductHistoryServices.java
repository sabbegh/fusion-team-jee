package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ProductHistory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ProductHistoryServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.ProductHistoryServicesRemote;

/**
 * Session Bean implementation class ProductHistoryServices
 */
@Stateless
public class ProductHistoryServices implements ProductHistoryServicesRemote,
		ProductHistoryServicesLocal {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public ProductHistoryServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Boolean addProductHistory(ProductHistory theProductHistory) {
		Boolean b = false;
		try {
			entityManager.persist(theProductHistory);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public ProductHistory findProductHistoryById(Long theId) {
		return entityManager.find(ProductHistory.class, theId);
	}

	@Override
	public Boolean updateProductHistory(ProductHistory theProductHistory) {
		boolean b = false;
		try {
			entityManager.merge(theProductHistory);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteProductHistory(ProductHistory theProductHistory) {
		Boolean b = false;
		try {
			theProductHistory = findProductHistoryById(theProductHistory
					.getProductHistoryId());
			entityManager.remove(theProductHistory);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteProductHistoryById(Long theId) {
		Boolean b = false;
		try {
			entityManager.remove(findProductHistoryById(theId));
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductHistory> findAllProductHistory() {
		Query query = entityManager
				.createQuery("select e from ProductHistory e ");
		return query.getResultList();
	}

	@Override
	public Long addProductHistoryAndReturnId(ProductHistory theProductHistory) {

		try {
			entityManager.persist(theProductHistory);
			entityManager.flush();
			return theProductHistory.getProductHistoryId();
		} catch (Exception e) {
		}
		return null;
	}

}
