package tn.edu.esprit.sigma.fusion.youbay.utilities;

import javax.ejb.Remote;

@Remote
public interface MessagingServicesRemote {
	void sendEmail(String to, String subject, String msg );
}
