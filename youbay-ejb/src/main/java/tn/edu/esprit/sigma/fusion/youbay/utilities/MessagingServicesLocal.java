package tn.edu.esprit.sigma.fusion.youbay.utilities;

import javax.ejb.Local;

@Local
public interface MessagingServicesLocal {
	void sendEmail(String to, String subject, String msg);
}
