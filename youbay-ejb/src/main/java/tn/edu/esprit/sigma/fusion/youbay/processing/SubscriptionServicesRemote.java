/*
 * 
 * Author Marwen
 */
package tn.edu.esprit.sigma.fusion.youbay.processing;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;

@Remote
public interface SubscriptionServicesRemote {
	void subscribeSeller(Seller seller);
	
	void subscribeBuyer(Buyer buyer);
}
