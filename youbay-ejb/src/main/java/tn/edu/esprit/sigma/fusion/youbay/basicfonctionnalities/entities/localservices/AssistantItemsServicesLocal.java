package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.AssistantItems;

@Local
public interface AssistantItemsServicesLocal {
	Boolean addAssistantItems(AssistantItems theAssistantItems);

	Long addAssistantItemsAndReturnId(AssistantItems theAssistantItems);

	AssistantItems findAssistantItemsById(Long theId);

	Boolean updateAssistantItems(AssistantItems theAssistantItems);

	Boolean deleteAssistantItems(AssistantItems theAssistantItems);

	Boolean deleteAssistantItemsById(Long theId);

	List<AssistantItems> findAllAssistantItems();

}
