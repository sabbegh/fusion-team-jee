package tn.edu.esprit.sigma.fusion.youbayclient.payment;

import java.math.BigDecimal;

import net.authorize.Environment;
import net.authorize.api.contract.v1.CreateTransactionRequest;
import net.authorize.api.contract.v1.CreateTransactionResponse;
import net.authorize.api.contract.v1.CreditCardType;
import net.authorize.api.contract.v1.CustomerAddressExType;
import net.authorize.api.contract.v1.CustomerDataType;
import net.authorize.api.contract.v1.CustomerTypeEnum;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessageTypeEnum;
import net.authorize.api.contract.v1.PaymentType;
import net.authorize.api.contract.v1.TransactionRequestType;
import net.authorize.api.contract.v1.TransactionResponse;
import net.authorize.api.contract.v1.TransactionTypeEnum;
import net.authorize.api.controller.CreateTransactionController;
import net.authorize.api.controller.base.ApiOperationBase;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;

public class ChargeCreditCard {

	//
	// Run this sample from command line with:
	// java -jar target/ChargeCreditCard-jar-with-dependencies.jar
	//
	public static void run(String apiLoginId, String transactionKey) {

		// Common code to set for all requests
		ApiOperationBase.setEnvironment(Environment.SANDBOX);

		MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
		merchantAuthenticationType.setName(apiLoginId);
		merchantAuthenticationType.setTransactionKey(transactionKey);
		ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

		// Populate the payment data
		PaymentType paymentType = new PaymentType();
		CreditCardType creditCard = new CreditCardType();
		creditCard.setCardNumber("4242424242424242");
		creditCard.setExpirationDate("0822");
		paymentType.setCreditCard(creditCard);

		// Create the payment transaction request
		TransactionRequestType txnRequest = new TransactionRequestType();
		txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());
		txnRequest.setPayment(paymentType);
		txnRequest.setAmount(new BigDecimal(500.00));

		// Make the API Request
		CreateTransactionRequest apiRequest = new CreateTransactionRequest();
		apiRequest.setTransactionRequest(txnRequest);
		CreateTransactionController controller = new CreateTransactionController(apiRequest);
		controller.execute();

		CreateTransactionResponse response = controller.getApiResponse();

		if (response != null) {

			// If API Response is ok, go ahead and check the transaction
			// response
			if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {

				TransactionResponse result = response.getTransactionResponse();
				if (result.getResponseCode().equals("1")) {
					System.out.println(result.getResponseCode());
					System.out.println("Successful Credit Card Transaction");
					System.out.println(result.getAuthCode());
					System.out.println(result.getTransId());
				} else {
					System.out.println("Failed Transaction" + result.getResponseCode());
				}
			} else {
				System.out.println("Failed Transaction:  " + response.getMessages().getResultCode());
			}
		}

	}

	//
	// Run this sample from command line with:
	// java -jar target/ChargeCreditCard-jar-with-dependencies.jar
	//
	public static void run(String apiLoginId, String transactionKey, BigDecimal amount, Buyer buyer, String CardNumber,
			String ExpirationDate) {

		// Common code to set for all requests
		ApiOperationBase.setEnvironment(Environment.SANDBOX);

		MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
		merchantAuthenticationType.setName(apiLoginId);
		merchantAuthenticationType.setTransactionKey(transactionKey);
		ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

		// Populate the payment data
		PaymentType paymentType = new PaymentType();
		CreditCardType creditCard = new CreditCardType();
		creditCard.setCardNumber(CardNumber);
		creditCard.setExpirationDate(ExpirationDate);

		paymentType.setCreditCard(creditCard);

		// Create the payment transaction request
		TransactionRequestType txnRequest = new TransactionRequestType();
		txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());
		txnRequest.setPayment(paymentType);
		txnRequest.setAmount(amount);
		CustomerDataType customerDataType = new CustomerDataType();
		customerDataType.setEmail(buyer.getEmail());
		customerDataType.setId((String) buyer.getYouBayUserId().toString());
		CustomerAddressExType customerAddressType = new CustomerAddressExType();
		customerAddressType.setAddress(buyer.getAddressLine1());
		customerAddressType.setCity(buyer.getAddressCity());
		customerAddressType.setState(buyer.getAddressCity());
		customerAddressType.setZip("2083");
		customerAddressType.setCountry(buyer.getCountryOfResidence());
		customerAddressType.setEmail(buyer.getEmail());
		customerAddressType.setPhoneNumber(buyer.getPhoneNumber());
		customerAddressType.setFaxNumber(buyer.getPhoneNumber());
		customerAddressType.setFirstName(buyer.getFirstName());
		customerAddressType.setLastName(buyer.getLastName());
		customerAddressType.setCompany("None");

		txnRequest.setBillTo(customerAddressType);
		txnRequest.setShipTo(customerAddressType);
		customerDataType.setType(CustomerTypeEnum.INDIVIDUAL);
		txnRequest.setCustomer(customerDataType);

		// Make the API Request
		CreateTransactionRequest apiRequest = new CreateTransactionRequest();
		apiRequest.setTransactionRequest(txnRequest);
		CreateTransactionController controller = new CreateTransactionController(apiRequest);
		controller.execute();

		CreateTransactionResponse response = controller.getApiResponse();

		if (response != null) {

			// If API Response is ok, go ahead and check the transaction
			// response
			if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {

				TransactionResponse result = response.getTransactionResponse();
				if (result.getResponseCode().equals("1")) {
					System.out.println(result.getResponseCode());
					System.out.println("Successful Credit Card Transaction");
					System.out.println(result.getAuthCode());
					System.out.println(result.getTransId());
				} else {
					System.out.println("Failed Transaction" + result.getResponseCode());
				}
			} else {
				System.out.println("Failed Transaction:  " + response.getMessages().getResultCode());
			}
		}

	}

}
