package tn.edu.esprit.sigma.fusion.youbayclient.payment;

import net.authorize.Environment;
import net.authorize.api.contract.v1.CreditCardType;
import net.authorize.api.contract.v1.CustomerAddressType;
import net.authorize.api.contract.v1.CustomerPaymentProfileExType;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessageTypeEnum;
import net.authorize.api.contract.v1.PaymentType;
import net.authorize.api.contract.v1.UpdateCustomerPaymentProfileRequest;
import net.authorize.api.contract.v1.UpdateCustomerPaymentProfileResponse;
import net.authorize.api.contract.v1.ValidationModeEnum;
import net.authorize.api.controller.UpdateCustomerPaymentProfileController;
import net.authorize.api.controller.base.ApiOperationBase;

public class UpdateCustomerPaymentProfile {

	public static void run(String apiLoginId, String transactionKey) {

		ApiOperationBase.setEnvironment(Environment.SANDBOX);

		MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
		merchantAuthenticationType.setName(apiLoginId);
		merchantAuthenticationType.setTransactionKey(transactionKey);
		ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

		// customer address
		CustomerAddressType customerAddress = new CustomerAddressType();
		customerAddress.setFirstName("John");
		customerAddress.setLastName("Doe");
		customerAddress.setAddress("123 Main Street");
		customerAddress.setCity("Bellevue");
		customerAddress.setState("WA");
		customerAddress.setZip("98004");
		customerAddress.setCountry("USA");
		customerAddress.setPhoneNumber("000-000-0000");

		// credit card details
		CreditCardType creditCard = new CreditCardType();
		creditCard.setCardNumber("4111111111111111");
		creditCard.setExpirationDate("2023-12");

		PaymentType paymentType = new PaymentType();
		paymentType.setCreditCard(creditCard);

		CustomerPaymentProfileExType customer = new CustomerPaymentProfileExType();
		customer.setPayment(paymentType);
		customer.setCustomerPaymentProfileId("20000");
		customer.setBillTo(customerAddress);

		UpdateCustomerPaymentProfileRequest apiRequest = new UpdateCustomerPaymentProfileRequest();
		apiRequest.setCustomerProfileId("10000");
		apiRequest.setPaymentProfile(customer);
		apiRequest.setValidationMode(ValidationModeEnum.LIVE_MODE);

		UpdateCustomerPaymentProfileController controller = new UpdateCustomerPaymentProfileController(apiRequest);
		controller.execute();

		UpdateCustomerPaymentProfileResponse response = new UpdateCustomerPaymentProfileResponse();
		response = controller.getApiResponse();

		if (response != null) {

			if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {

				System.out.println(response.getMessages().getMessage().get(0).getCode());
				System.out.println(response.getMessages().getMessage().get(0).getText());
			} else {
				System.out.println(
						"Failed to update customer payment profile:  " + response.getMessages().getResultCode());
			}
		}
	}
}