package tn.edu.esprit.sigma.fusion.youbayclient.crud;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.InitDatabaseServicesDelegate;

public class InitDatabaseServicesTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		InitDatabaseServicesDelegate.doTruncateAllTables();
		InitDatabaseServicesDelegate.doPopulateDatabase();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDoTruncateAllTables() {
		InitDatabaseServicesDelegate.doTruncateAllTables();
	}

	@Test
	public void testDoPopulateDatabase() {
		InitDatabaseServicesDelegate.doPopulateDatabase();
	}

}
