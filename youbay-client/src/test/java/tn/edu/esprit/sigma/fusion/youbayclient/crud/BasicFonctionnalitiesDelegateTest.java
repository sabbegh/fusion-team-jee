package tn.edu.esprit.sigma.fusion.youbayclient.crud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.AssistantItems;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Auction;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ClientType;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.CustomizedAds;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViews;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViewsId;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Manager;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReviewId;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ProductHistory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.SpecialPromotion;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.BasicFonctionnalitiesDelegate;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.InitDatabaseServicesDelegate;

public class BasicFonctionnalitiesDelegateTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		InitDatabaseServicesDelegate.doTruncateAllTables();

		/*
		 * We assume that doPopulateDatabase adds at least one instance of each
		 * JPA entity
		 */
		InitDatabaseServicesDelegate.doPopulateDatabase();
	}

	@After
	public void tearDown() throws Exception {
	}

	public String randomString() {
		return UUID.randomUUID().toString();
	}

	public Float randomFloat() {
		Random random = new Random();
		return random.nextFloat() * (10000);
	}

	@Test
	public void testDoAddAssistantItems() {

		String beacon = randomString();

		AssistantItems newItem = new AssistantItems();

		newItem.setQuestionText(beacon);

		BasicFonctionnalitiesDelegate.doAddAssistantItems(newItem);

		List<AssistantItems> list = BasicFonctionnalitiesDelegate.doFindAllAssistantItems();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (AssistantItems item : list) {
			if (item.getQuestionText().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoAddAssistantItemsAndReturnId() {

		String beacon = randomString();

		AssistantItems newItem = new AssistantItems();

		newItem.setQuestionText(beacon);
		assertNotEquals(null, BasicFonctionnalitiesDelegate.doAddAssistantItemsAndReturnId(newItem));
		List<AssistantItems> list = BasicFonctionnalitiesDelegate.doFindAllAssistantItems();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (AssistantItems item : list) {
			if (item.getQuestionText().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoFindAssistantItemsById() {
		List<AssistantItems> list = BasicFonctionnalitiesDelegate.doFindAllAssistantItems();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		long theId = list.get(0).getAssistantItemsId();

		AssistantItems item = BasicFonctionnalitiesDelegate.doFindAssistantItemsById(theId);

		assertNotEquals(item, null);

	}

	@Test
	public void testDoUpdateAssistantItems() {
		List<AssistantItems> list = BasicFonctionnalitiesDelegate.doFindAllAssistantItems();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		AssistantItems item = list.get(0);

		String beacon = randomString();

		item.setQuestionText(beacon);

		BasicFonctionnalitiesDelegate.doUpdateAssistantItems(item);

		AssistantItems updatedItem = BasicFonctionnalitiesDelegate.doFindAssistantItemsById(item.getAssistantItemsId());

		assertEquals(beacon, updatedItem.getQuestionText());
	}

	@Test
	public void testDoDeleteAssistantItems() {
		List<AssistantItems> list = BasicFonctionnalitiesDelegate.doFindAllAssistantItems();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		AssistantItems item = list.get(0);
		Long theId = item.getAssistantItemsId();
		BasicFonctionnalitiesDelegate.doDeleteAssistantItems(item);

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindAssistantItemsById(theId));

	}

	@Test
	public void testDoDeleteAssistantItemsById() {
		List<AssistantItems> list = BasicFonctionnalitiesDelegate.doFindAllAssistantItems();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		AssistantItems item = list.get(0);
		Long theId = item.getAssistantItemsId();
		BasicFonctionnalitiesDelegate.doDeleteAssistantItemsById(item.getAssistantItemsId());

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindAssistantItemsById(theId));
	}

	@Test
	public void testDoFindAllAssistantItems() {
		List<AssistantItems> list = BasicFonctionnalitiesDelegate.doFindAllAssistantItems();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddAuction() {

		Float beacon = randomFloat();

		Auction newItem = new Auction();

		newItem.setCurrentPrice(beacon);

		BasicFonctionnalitiesDelegate.doAddAuction(newItem);

		List<Auction> list = BasicFonctionnalitiesDelegate.doFindAllAuction();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Auction item : list) {
			if (item.getCurrentPrice().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false);

	}

	@Test
	public void testDoAddAuctionAndReturnId() {

		Float beacon = randomFloat();

		Auction newItem = new Auction();

		newItem.setCurrentPrice(beacon);

		assertNotEquals(null, BasicFonctionnalitiesDelegate.doAddAuctionAndReturnId(newItem));
		List<Auction> list = BasicFonctionnalitiesDelegate.doFindAllAuction();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Auction item : list) {
			if (item.getCurrentPrice().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false);

	}

	@Test
	public void testDoFindAuctionById() {
		List<Auction> list = BasicFonctionnalitiesDelegate.doFindAllAuction();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		long theId = list.get(0).getAuctionId();

		Auction item = BasicFonctionnalitiesDelegate.doFindAuctionById(theId);

		assertNotEquals(item, null);

	}

	@Test
	public void testDoUpdateAuction() {
		List<Auction> list = BasicFonctionnalitiesDelegate.doFindAllAuction();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Auction item = list.get(0);

		Float beacon = randomFloat();
		item.setCurrentPrice(beacon);

		BasicFonctionnalitiesDelegate.doUpdateAuction(item);

		Auction updatedItem = BasicFonctionnalitiesDelegate.doFindAuctionById(item.getAuctionId());

		assertEquals(beacon, updatedItem.getCurrentPrice(), 0.2);

	}

	@Test
	public void testDoDeleteAuction() {
		List<Auction> list = BasicFonctionnalitiesDelegate.doFindAllAuction();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Auction item = list.get(0);
		Long theId = item.getAuctionId();
		BasicFonctionnalitiesDelegate.doDeleteAuction(item);

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindAuctionById(theId));

	}

	@Test
	public void testDoDeleteAuctionById() {
		List<Auction> list = BasicFonctionnalitiesDelegate.doFindAllAuction();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Auction item = list.get(0);
		Long theId = item.getAuctionId();
		BasicFonctionnalitiesDelegate.doDeleteAuctionById(item.getAuctionId());

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindAuctionById(theId));
	}

	@Test
	public void testDoFindAllAuction() {
		List<Auction> list = BasicFonctionnalitiesDelegate.doFindAllAuction();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddBuyer() {

		String beacon = randomString();

		Buyer newItem = new Buyer();

		newItem.setAddressCity(beacon);

		BasicFonctionnalitiesDelegate.doAddBuyer(newItem);

		List<Buyer> list = BasicFonctionnalitiesDelegate.doFindAllBuyer();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Buyer item : list) {
			if (item.getAddressCity().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoAddBuyerAndReturnId() {

		String beacon = randomString();

		Buyer newItem = new Buyer();

		newItem.setAddressCity(beacon);

		assertNotEquals(null, BasicFonctionnalitiesDelegate.doAddBuyerAndReturnId(newItem));
		List<Buyer> list = BasicFonctionnalitiesDelegate.doFindAllBuyer();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Buyer item : list) {
			if (item.getAddressCity().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoFindBuyerById() {
		List<Buyer> list = BasicFonctionnalitiesDelegate.doFindAllBuyer();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		long theId = list.get(0).getYouBayUserId();

		Buyer item = BasicFonctionnalitiesDelegate.doFindBuyerById(theId);

		assertNotEquals(item, null);

	}

	@Test
	public void testDoUpdateBuyer() {
		List<Buyer> list = BasicFonctionnalitiesDelegate.doFindAllBuyer();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Buyer item = list.get(0);

		String beacon = randomString();

		item.setAddressCity(beacon);

		BasicFonctionnalitiesDelegate.doUpdateBuyer(item);

		Buyer updatedItem = BasicFonctionnalitiesDelegate.doFindBuyerById(item.getYouBayUserId());

		assertEquals(beacon, updatedItem.getAddressCity());
	}

	@Test
	public void testDoDeleteBuyer() {
		List<Buyer> list = BasicFonctionnalitiesDelegate.doFindAllBuyer();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Buyer item = list.get(0);
		Long theId = item.getYouBayUserId();
		BasicFonctionnalitiesDelegate.doDeleteBuyer(item);

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindBuyerById(theId));

	}

	@Test
	public void testDoDeleteBuyerById() {
		List<Buyer> list = BasicFonctionnalitiesDelegate.doFindAllBuyer();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Buyer item = list.get(0);
		Long theId = item.getYouBayUserId();
		BasicFonctionnalitiesDelegate.doDeleteBuyerById(item.getYouBayUserId());

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindBuyerById(theId));
	}

	@Test
	public void testDoFindAllBuyer() {
		List<Buyer> list = BasicFonctionnalitiesDelegate.doFindAllBuyer();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddCategory() {

		String beacon = randomString();

		Category newItem = new Category();

		newItem.setCategoryName(beacon);

		BasicFonctionnalitiesDelegate.doAddCategory(newItem);

		List<Category> list = BasicFonctionnalitiesDelegate.doFindAllCategory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Category item : list) {
			if (item.getCategoryName().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoAddCategoryAndReturnId() {

		String beacon = randomString();

		Category newItem = new Category();

		newItem.setCategoryName(beacon);

		assertNotEquals(null, BasicFonctionnalitiesDelegate.doAddCategoryAndReturnId(newItem));

		List<Category> list = BasicFonctionnalitiesDelegate.doFindAllCategory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Category item : list) {
			if (item.getCategoryName().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoFindCategoryById() {
		List<Category> list = BasicFonctionnalitiesDelegate.doFindAllCategory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		long theId = list.get(0).getCategoryId();

		Category item = BasicFonctionnalitiesDelegate.doFindCategoryById(theId);

		assertNotEquals(item, null);

	}

	@Test
	public void testDoUpdateCategory() {
		List<Category> list = BasicFonctionnalitiesDelegate.doFindAllCategory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Category item = list.get(0);

		String beacon = randomString();

		item.setCategoryName(beacon);

		BasicFonctionnalitiesDelegate.doUpdateCategory(item);

		Category updatedItem = BasicFonctionnalitiesDelegate.doFindCategoryById(item.getCategoryId());

		assertEquals(beacon, updatedItem.getCategoryName());
	}

	@Test
	public void testDoDeleteCategory() {
		List<Category> list = BasicFonctionnalitiesDelegate.doFindAllCategory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Category item = list.get(0);
		Long theId = item.getCategoryId();
		BasicFonctionnalitiesDelegate.doDeleteCategory(item);

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindCategoryById(theId));

	}

	@Test
	public void testDoDeleteCategoryById() {
		List<Category> list = BasicFonctionnalitiesDelegate.doFindAllCategory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Category item = list.get(0);
		Long theId = item.getCategoryId();
		BasicFonctionnalitiesDelegate.doDeleteCategoryById(item.getCategoryId());

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindCategoryById(theId));
	}

	@Test
	public void testDoFindAllCategory() {
		List<Category> list = BasicFonctionnalitiesDelegate.doFindAllCategory();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddCustomizedAds() {

		String beacon = randomString();

		CustomizedAds newItem = new CustomizedAds();

		newItem.setCustomizedMessage(beacon);

		BasicFonctionnalitiesDelegate.doAddCustomizedAds(newItem);

		List<CustomizedAds> list = BasicFonctionnalitiesDelegate.doFindAllCustomizedAds();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (CustomizedAds item : list) {
			if (item.getCustomizedMessage().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoAddCustomizedAdsAndReturnId() {

		String beacon = randomString();

		CustomizedAds newItem = new CustomizedAds();

		newItem.setCustomizedMessage(beacon);

		BasicFonctionnalitiesDelegate.doAddCustomizedAdsAndReturnId(newItem);

		List<CustomizedAds> list = BasicFonctionnalitiesDelegate.doFindAllCustomizedAds();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (CustomizedAds item : list) {
			if (item.getCustomizedMessage().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoFindCustomizedAdsById() {
		List<CustomizedAds> list = BasicFonctionnalitiesDelegate.doFindAllCustomizedAds();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		long theId = list.get(0).getCustomizedAdsId();

		CustomizedAds item = BasicFonctionnalitiesDelegate.doFindCustomizedAdsById(theId);

		assertNotEquals(item, null);

	}

	@Test
	public void testDoUpdateCustomizedAds() {
		List<CustomizedAds> list = BasicFonctionnalitiesDelegate.doFindAllCustomizedAds();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		CustomizedAds item = list.get(0);

		String beacon = randomString();

		item.setCustomizedMessage(beacon);

		BasicFonctionnalitiesDelegate.doUpdateCustomizedAds(item);

		CustomizedAds updatedItem = BasicFonctionnalitiesDelegate.doFindCustomizedAdsById(item.getCustomizedAdsId());

		assertEquals(beacon, updatedItem.getCustomizedMessage());
	}

	@Test
	public void testDoDeleteCustomizedAds() {
		List<CustomizedAds> list = BasicFonctionnalitiesDelegate.doFindAllCustomizedAds();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		CustomizedAds item = list.get(0);
		Long theId = item.getCustomizedAdsId();
		BasicFonctionnalitiesDelegate.doDeleteCustomizedAds(item);

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindCustomizedAdsById(theId));

	}

	@Test
	public void testDoDeleteCustomizedAdsById() {
		List<CustomizedAds> list = BasicFonctionnalitiesDelegate.doFindAllCustomizedAds();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		CustomizedAds item = list.get(0);
		Long theId = item.getCustomizedAdsId();
		BasicFonctionnalitiesDelegate.doDeleteCustomizedAdsById(item.getCustomizedAdsId());

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindCustomizedAdsById(theId));
	}

	@Test
	public void testDoFindAllCustomizedAds() {
		List<CustomizedAds> list = BasicFonctionnalitiesDelegate.doFindAllCustomizedAds();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddHistoryOfViews() {

		List<Buyer> listBuyers = BasicFonctionnalitiesDelegate.doFindAllBuyer();
		List<Product> listProducts = BasicFonctionnalitiesDelegate.doFindAllProduct();

		// We suppose populateDatabase added at least 2 buyers and 3 products
		assertTrue(listBuyers.size() >= 2);
		assertTrue(listProducts.size() >= 3);

		Date randomDate = new Date();

		randomDate.setTime(randomDate.getTime() - 7657000); // Just a random
															// test to avoid
															// conflicts when
															// running JUnit
															// tests
															// concurrently

		HistoryOfViews historyItem1 = new HistoryOfViews(randomString(), ClientType.other, listBuyers.get(0),
				listProducts.get(0), randomDate);
		HistoryOfViews historyItem2 = new HistoryOfViews(randomString(), ClientType.other, listBuyers.get(0),
				listProducts.get(1), randomDate);
		HistoryOfViews historyItem3 = new HistoryOfViews(randomString(), ClientType.other, listBuyers.get(1),
				listProducts.get(0), randomDate);
		HistoryOfViews historyItem4 = new HistoryOfViews(randomString(), ClientType.other, listBuyers.get(1),
				listProducts.get(2), randomDate);

		BasicFonctionnalitiesDelegate.doAddHistoryOfViews(historyItem1);
		BasicFonctionnalitiesDelegate.doAddHistoryOfViews(historyItem2);
		BasicFonctionnalitiesDelegate.doAddHistoryOfViews(historyItem3);
		BasicFonctionnalitiesDelegate.doAddHistoryOfViews(historyItem4);
	}

	@Test
	public void testDoFindHistoryOfViewsById() {

		List<HistoryOfViews> list = BasicFonctionnalitiesDelegate.doFindAllHistoryOfViews();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);

		HistoryOfViews history = list.get(0);

		HistoryOfViewsId id = history.getHistoryOfViewsId();

		HistoryOfViews found = BasicFonctionnalitiesDelegate.doFindHistoryOfViewsById(id);

		assertNotEquals(null, found);

	}

	@Test
	public void testDoUpdateHistoryOfViews() {

		List<HistoryOfViews> list = BasicFonctionnalitiesDelegate.doFindAllHistoryOfViews();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);

		HistoryOfViews history = list.get(0);

		HistoryOfViewsId id = history.getHistoryOfViewsId();

		String newComment = randomString();

		history.setComment(newComment);

		BasicFonctionnalitiesDelegate.doUpdateHistoryOfViews(history);

		HistoryOfViews found = BasicFonctionnalitiesDelegate.doFindHistoryOfViewsById(id);

		assertEquals(newComment, found.getComment());
	}

	@Test
	public void testDoDeleteHistoryOfViewsById() {

		List<HistoryOfViews> list = BasicFonctionnalitiesDelegate.doFindAllHistoryOfViews();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);

		HistoryOfViews history = list.get(0);

		HistoryOfViewsId id = history.getHistoryOfViewsId();

		BasicFonctionnalitiesDelegate.doDeleteHistoryOfViewsById(id);

		HistoryOfViews found = BasicFonctionnalitiesDelegate.doFindHistoryOfViewsById(id);

		assertEquals(null, found);
	}

	@Test
	public void testDoFindAllHistoryOfViews() {

		List<HistoryOfViews> list = BasicFonctionnalitiesDelegate.doFindAllHistoryOfViews();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddManager() {

		String beacon = randomString();

		Manager newItem = new Manager();

		newItem.setFirstName(beacon);

		BasicFonctionnalitiesDelegate.doAddManager(newItem);

		List<Manager> list = BasicFonctionnalitiesDelegate.doFindAllManager();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Manager item : list) {
			if (item.getFirstName().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoAddManagerAndReturnId() {

		String beacon = randomString();

		Manager newItem = new Manager();

		newItem.setFirstName(beacon);

		assertNotEquals(null, BasicFonctionnalitiesDelegate.doAddManagerAndReturnId(newItem));

		List<Manager> list = BasicFonctionnalitiesDelegate.doFindAllManager();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Manager item : list) {
			if (item.getFirstName().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoFindManagerById() {
		List<Manager> list = BasicFonctionnalitiesDelegate.doFindAllManager();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		long theId = list.get(0).getYouBayUserId();

		Manager item = BasicFonctionnalitiesDelegate.doFindManagerById(theId);

		assertNotEquals(item, null);

	}

	@Test
	public void testDoUpdateManager() {
		List<Manager> list = BasicFonctionnalitiesDelegate.doFindAllManager();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Manager item = list.get(0);

		String beacon = randomString();

		item.setFirstName(beacon);

		BasicFonctionnalitiesDelegate.doUpdateManager(item);

		Manager updatedItem = BasicFonctionnalitiesDelegate.doFindManagerById(item.getYouBayUserId());

		assertEquals(beacon, updatedItem.getFirstName());
	}

	@Test
	public void testDoDeleteManager() {
		List<Manager> list = BasicFonctionnalitiesDelegate.doFindAllManager();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Manager item = list.get(0);
		Long theId = item.getYouBayUserId();
		BasicFonctionnalitiesDelegate.doDeleteManager(item);

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindManagerById(theId));

	}

	@Test
	public void testDoDeleteManagerById() {
		List<Manager> list = BasicFonctionnalitiesDelegate.doFindAllManager();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Manager item = list.get(0);
		Long theId = item.getYouBayUserId();
		BasicFonctionnalitiesDelegate.doDeleteManagerById(item.getYouBayUserId());

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindManagerById(theId));
	}

	@Test
	public void testDoFindAllManager() {
		List<Manager> list = BasicFonctionnalitiesDelegate.doFindAllManager();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddOrderAndReview() {
		List<Buyer> listBuyers = BasicFonctionnalitiesDelegate.doFindAllBuyer();
		List<Product> listProducts = BasicFonctionnalitiesDelegate.doFindAllProduct();

		// We suppose populateDatabase added at least 2 buyers and 3 products
		assertTrue(listBuyers.size() >= 2);
		assertTrue(listProducts.size() >= 3);

		Date randomDate = new Date();

		randomDate.setTime(randomDate.getTime() - 3657000); // Just a random
															// test to avoid
															// conflicts when
															// running JUnit
															// tests
															// concurrently

		OrderAndReview order1 = new OrderAndReview(15f, "Please package the items carefuly !", false, false, randomDate,
				false, false, null, null, null, listBuyers.get(0), listProducts.get(0));

		OrderAndReview order2 = new OrderAndReview(15f, "Please package the items carefuly !", false, false, randomDate,
				false, false, null, null, null, listBuyers.get(1), listProducts.get(1));

		BasicFonctionnalitiesDelegate.doAddOrderAndReview(order1);
		BasicFonctionnalitiesDelegate.doAddOrderAndReview(order2);
	}

	@Test
	public void testDoUpdateOrderAndReview() {

		List<OrderAndReview> list = BasicFonctionnalitiesDelegate.doFindAllOrderAndReview();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);

		OrderAndReview order = list.get(0);

		OrderAndReviewId id = order.getOrderAndReviewId();

		String newString = randomString();

		order.setInitialMessageToSeller(newString);

		BasicFonctionnalitiesDelegate.doUpdateOrderAndReview(order);

		OrderAndReview found = BasicFonctionnalitiesDelegate.doFindOrderAndReviewById(id);

		assertEquals(newString, found.getInitialMessageToSeller());
	}

	@Test
	public void testDoFindOrderAndReviewById() {

		List<OrderAndReview> list = BasicFonctionnalitiesDelegate.doFindAllOrderAndReview();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);

		OrderAndReview order = list.get(0);

		OrderAndReviewId id = order.getOrderAndReviewId();

		OrderAndReview found = BasicFonctionnalitiesDelegate.doFindOrderAndReviewById(id);

		assertNotEquals(null, found);
	}

	@Test
	public void testDoDeleteOrderAndReviewById() {

		List<OrderAndReview> list = BasicFonctionnalitiesDelegate.doFindAllOrderAndReview();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);

		OrderAndReview order = list.get(0);

		OrderAndReviewId id = order.getOrderAndReviewId();

		BasicFonctionnalitiesDelegate.doDeleteOrderAndReviewById(id);

		OrderAndReview found = BasicFonctionnalitiesDelegate.doFindOrderAndReviewById(id);

		assertEquals(null, found);
	}

	@Test
	public void testDoFindAllOrderAndReview() {

		List<OrderAndReview> list = BasicFonctionnalitiesDelegate.doFindAllOrderAndReview();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddProductHistory() {

		String beacon = randomString();

		ProductHistory newItem = new ProductHistory();

		newItem.setProductShortDescriptionHistory(beacon);

		BasicFonctionnalitiesDelegate.doAddProductHistory(newItem);

		List<ProductHistory> list = BasicFonctionnalitiesDelegate.doFindAllProductHistory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (ProductHistory item : list) {
			if (item.getProductShortDescriptionHistory().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoAddProductHistoryAndReturnId() {

		String beacon = randomString();

		ProductHistory newItem = new ProductHistory();

		newItem.setProductShortDescriptionHistory(beacon);

		assertNotEquals(null, BasicFonctionnalitiesDelegate.doAddProductHistoryAndReturnId(newItem));

		List<ProductHistory> list = BasicFonctionnalitiesDelegate.doFindAllProductHistory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (ProductHistory item : list) {
			if (item.getProductShortDescriptionHistory().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoFindProductHistoryById() {
		List<ProductHistory> list = BasicFonctionnalitiesDelegate.doFindAllProductHistory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		long theId = list.get(0).getProductHistoryId();

		ProductHistory item = BasicFonctionnalitiesDelegate.doFindProductHistoryById(theId);

		assertNotEquals(item, null);

	}

	@Test
	public void testDoUpdateProductHistory() {
		List<ProductHistory> list = BasicFonctionnalitiesDelegate.doFindAllProductHistory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		ProductHistory item = list.get(0);

		String beacon = randomString();

		item.setProductShortDescriptionHistory(beacon);

		BasicFonctionnalitiesDelegate.doUpdateProductHistory(item);

		ProductHistory updatedItem = BasicFonctionnalitiesDelegate.doFindProductHistoryById(item.getProductHistoryId());

		assertEquals(beacon, updatedItem.getProductShortDescriptionHistory());
	}

	@Test
	public void testDoDeleteProductHistory() {
		List<ProductHistory> list = BasicFonctionnalitiesDelegate.doFindAllProductHistory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		ProductHistory item = list.get(0);
		Long theId = item.getProductHistoryId();
		BasicFonctionnalitiesDelegate.doDeleteProductHistory(item);

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindProductHistoryById(theId));

	}

	@Test
	public void testDoDeleteProductHistoryById() {
		List<ProductHistory> list = BasicFonctionnalitiesDelegate.doFindAllProductHistory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		ProductHistory item = list.get(0);
		Long theId = item.getProductHistoryId();
		BasicFonctionnalitiesDelegate.doDeleteProductHistoryById(item.getProductHistoryId());

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindProductHistoryById(theId));
	}

	@Test
	public void testDoFindAllProductHistory() {
		List<ProductHistory> list = BasicFonctionnalitiesDelegate.doFindAllProductHistory();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddProduct() {

		String beacon = randomString();

		Product newItem = new Product();

		newItem.setProductDescription(beacon);

		BasicFonctionnalitiesDelegate.doAddProduct(newItem);

		List<Product> list = BasicFonctionnalitiesDelegate.doFindAllProduct();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Product item : list) {
			if (item.getProductDescription().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false);

	}

	@Test
	public void testDoAddProductAndReturnId() {

		String beacon = randomString();

		Product newItem = new Product();

		newItem.setProductDescription(beacon);

		assertNotEquals(null, BasicFonctionnalitiesDelegate.doAddProductAndReturnId(newItem));

		List<Product> list = BasicFonctionnalitiesDelegate.doFindAllProduct();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Product item : list) {
			if (item.getProductDescription().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false);

	}

	@Test
	public void testDoFindProductById() {
		List<Product> list = BasicFonctionnalitiesDelegate.doFindAllProduct();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		long theId = list.get(0).getProductId();

		Product item = BasicFonctionnalitiesDelegate.doFindProductById(theId);

		assertNotEquals(item, null);

	}

	@Test
	public void testDoUpdateProduct() {
		List<Product> list = BasicFonctionnalitiesDelegate.doFindAllProduct();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Product item = list.get(0);

		String beacon = randomString();

		item.setProductDescription(beacon);

		BasicFonctionnalitiesDelegate.doUpdateProduct(item);

		Product updatedItem = BasicFonctionnalitiesDelegate.doFindProductById(item.getProductId());

		assertEquals(beacon, updatedItem.getProductDescription());
	}

	@Test
	public void testDoDeleteProduct() {
		List<Product> list = BasicFonctionnalitiesDelegate.doFindAllProduct();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Product item = list.get(0);
		Long theId = item.getProductId();
		BasicFonctionnalitiesDelegate.doDeleteProduct(item);

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindProductById(theId));

	}

	@Test
	public void testDoDeleteProductById() {
		List<Product> list = BasicFonctionnalitiesDelegate.doFindAllProduct();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Product item = list.get(0);
		Long theId = item.getProductId();
		BasicFonctionnalitiesDelegate.doDeleteProductById(item.getProductId());

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindProductById(theId));
	}

	@Test
	public void testDoFindAllProduct() {
		List<Product> list = BasicFonctionnalitiesDelegate.doFindAllProduct();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddSeller() {

		String beacon = randomString();

		Seller newItem = new Seller();

		newItem.setCountryOfResidence(beacon);

		BasicFonctionnalitiesDelegate.doAddSeller(newItem);

		List<Seller> list = BasicFonctionnalitiesDelegate.doFindAllSeller();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Seller item : list) {
			if (item.getCountryOfResidence().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoAddSellerAndReturnId() {

		String beacon = randomString();

		Seller newItem = new Seller();

		newItem.setCountryOfResidence(beacon);

		assertNotEquals(null, BasicFonctionnalitiesDelegate.doAddSellerAndReturnId(newItem));

		List<Seller> list = BasicFonctionnalitiesDelegate.doFindAllSeller();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Seller item : list) {
			if (item.getCountryOfResidence().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoFindSellerById() {
		List<Seller> list = BasicFonctionnalitiesDelegate.doFindAllSeller();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		long theId = list.get(0).getYouBayUserId();

		Seller item = BasicFonctionnalitiesDelegate.doFindSellerById(theId);

		assertNotEquals(item, null);

	}

	@Test
	public void testDoUpdateSeller() {
		List<Seller> list = BasicFonctionnalitiesDelegate.doFindAllSeller();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Seller item = list.get(0);

		String beacon = randomString();

		item.setCountryOfResidence(beacon);

		BasicFonctionnalitiesDelegate.doUpdateSeller(item);

		Seller updatedItem = BasicFonctionnalitiesDelegate.doFindSellerById(item.getYouBayUserId());

		assertEquals(beacon, updatedItem.getCountryOfResidence());
	}

	@Test
	public void testDoDeleteSeller() {
		List<Seller> list = BasicFonctionnalitiesDelegate.doFindAllSeller();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Seller item = list.get(0);
		Long theId = item.getYouBayUserId();
		BasicFonctionnalitiesDelegate.doDeleteSeller(item);

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindSellerById(theId));

	}

	@Test
	public void testDoDeleteSellerById() {
		List<Seller> list = BasicFonctionnalitiesDelegate.doFindAllSeller();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Seller item = list.get(0);
		Long theId = item.getYouBayUserId();
		BasicFonctionnalitiesDelegate.doDeleteSellerById(item.getYouBayUserId());

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindSellerById(theId));
	}

	@Test
	public void testDoFindAllSeller() {
		List<Seller> list = BasicFonctionnalitiesDelegate.doFindAllSeller();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddSpecialPromotion() {

		String beacon = randomString();

		SpecialPromotion newItem = new SpecialPromotion();

		newItem.setDealDescription(beacon);

		BasicFonctionnalitiesDelegate.doAddSpecialPromotion(newItem);

		List<SpecialPromotion> list = BasicFonctionnalitiesDelegate.doFindAllSpecialPromotion();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (SpecialPromotion item : list) {
			if (item.getDealDescription().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoAddSpecialPromotionAndReturnId() {

		String beacon = randomString();

		SpecialPromotion newItem = new SpecialPromotion();

		newItem.setDealDescription(beacon);

		BasicFonctionnalitiesDelegate.doAddSpecialPromotionAndReturnId(newItem);

		List<SpecialPromotion> list = BasicFonctionnalitiesDelegate.doFindAllSpecialPromotion();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (SpecialPromotion item : list) {
			if (item.getDealDescription().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoFindSpecialPromotionById() {
		List<SpecialPromotion> list = BasicFonctionnalitiesDelegate.doFindAllSpecialPromotion();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		long theId = list.get(0).getSpecialPromotionId();

		SpecialPromotion item = BasicFonctionnalitiesDelegate.doFindSpecialPromotionById(theId);

		assertNotEquals(item, null);

	}

	@Test
	public void testDoUpdateSpecialPromotion() {
		List<SpecialPromotion> list = BasicFonctionnalitiesDelegate.doFindAllSpecialPromotion();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		SpecialPromotion item = list.get(0);

		String beacon = randomString();

		item.setDealDescription(beacon);

		BasicFonctionnalitiesDelegate.doUpdateSpecialPromotion(item);

		SpecialPromotion updatedItem = BasicFonctionnalitiesDelegate
				.doFindSpecialPromotionById(item.getSpecialPromotionId());

		assertEquals(beacon, updatedItem.getDealDescription());
	}

	@Test
	public void testDoDeleteSpecialPromotion() {
		List<SpecialPromotion> list = BasicFonctionnalitiesDelegate.doFindAllSpecialPromotion();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		SpecialPromotion item = list.get(0);
		Long theId = item.getSpecialPromotionId();
		BasicFonctionnalitiesDelegate.doDeleteSpecialPromotion(item);

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindSpecialPromotionById(theId));

	}

	@Test
	public void testDoDeleteSpecialPromotionById() {
		List<SpecialPromotion> list = BasicFonctionnalitiesDelegate.doFindAllSpecialPromotion();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		SpecialPromotion item = list.get(0);
		Long theId = item.getSpecialPromotionId();
		BasicFonctionnalitiesDelegate.doDeleteSpecialPromotionById(item.getSpecialPromotionId());

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindSpecialPromotionById(theId));
	}

	@Test
	public void testDoFindAllSpecialPromotion() {
		List<SpecialPromotion> list = BasicFonctionnalitiesDelegate.doFindAllSpecialPromotion();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddSubcategory() {

		String beacon = randomString();

		Subcategory newItem = new Subcategory();

		newItem.setCategoryName(beacon);

		BasicFonctionnalitiesDelegate.doAddSubcategory(newItem);

		List<Subcategory> list = BasicFonctionnalitiesDelegate.doFindAllSubcategory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Subcategory item : list) {
			if (item.getCategoryName().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoAddSubcategoryAndReturnId() {

		String beacon = randomString();

		Subcategory newItem = new Subcategory();

		newItem.setCategoryName(beacon.substring(0, 30));

		assertNotEquals(null, BasicFonctionnalitiesDelegate.doAddSubcategoryAndReturnId(newItem));

		List<Subcategory> list = BasicFonctionnalitiesDelegate.doFindAllSubcategory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		for (Subcategory item : list) {
			if (item.getCategoryName().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this
										// means the
		// newItem was not found.

	}

	@Test
	public void testDoFindSubcategoryById() {
		List<Subcategory> list = BasicFonctionnalitiesDelegate.doFindAllSubcategory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		long theId = list.get(0).getSubcategoryId();

		Subcategory item = BasicFonctionnalitiesDelegate.doFindSubcategoryById(theId);

		assertNotEquals(item, null);

	}

	@Test
	public void testDoUpdateSubcategory() {
		List<Subcategory> list = BasicFonctionnalitiesDelegate.doFindAllSubcategory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Subcategory item = list.get(0);

		String beacon = randomString();

		item.setCategoryName(beacon);

		BasicFonctionnalitiesDelegate.doUpdateSubcategory(item);

		Subcategory updatedItem = BasicFonctionnalitiesDelegate.doFindSubcategoryById(item.getSubcategoryId());

		assertEquals(beacon, updatedItem.getCategoryName());
	}

	@Test
	public void testDoDeleteSubcategory() {
		List<Subcategory> list = BasicFonctionnalitiesDelegate.doFindAllSubcategory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Subcategory item = list.get(0);
		Long theId = item.getSubcategoryId();
		BasicFonctionnalitiesDelegate.doDeleteSubcategory(item);

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindSubcategoryById(theId));

	}

	@Test
	public void testDoDeleteSubcategoryById() {
		List<Subcategory> list = BasicFonctionnalitiesDelegate.doFindAllSubcategory();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		Subcategory item = list.get(0);
		Long theId = item.getSubcategoryId();
		BasicFonctionnalitiesDelegate.doDeleteSubcategoryById(item.getSubcategoryId());

		assertEquals(null, BasicFonctionnalitiesDelegate.doFindSubcategoryById(theId));
	}

	@Test
	public void testDoFindAllSubcategory() {
		List<Subcategory> list = BasicFonctionnalitiesDelegate.doFindAllSubcategory();

		assertNotEquals(list, null);
		assertNotEquals(list.size(), 0);
	}
}