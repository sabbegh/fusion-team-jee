package tn.edu.esprit.sigma.fusion.youbaywar.managedbeans.utilities;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Manager;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.BuyerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ManagerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SellerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.processing.EncryptionServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.utilities.MessagingServicesLocal;

@ManagedBean
@RequestScoped
public class Subscribe {

	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;

	private String countryOfResidence = "Tunisia";
	private Boolean isActive = false;
	private Boolean isBanned = false;
	private String password;
	private String type;

	@EJB
	EncryptionServicesLocal encryptionServicesLocal;
	@EJB
	ManagerServicesLocal managerServicesLocal;
	@EJB
	SellerServicesLocal sellerServicesLocal;
	@EJB
	BuyerServicesLocal buyerServicesLocal;
	@EJB
	MessagingServicesLocal messagingServicesLocal;

	public String subs() {

		String navigateTo = "/login?faces-redirect=true";

		if (type.equals("m")) {
			Manager user = new Manager();

			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmail(email);
			user.setPhoneNumber(phoneNumber);
			user.setCountryOfResidence(countryOfResidence);
			user.setIsActive(isActive);
			user.setIsBanned(isBanned);
			user.setPassword(encryptionServicesLocal.encryptString(password));
			managerServicesLocal.addManager(user);

			messagingServicesLocal.sendEmail(email, "WELCOME TO YOU BAY", "WELCOME TO YOU BAY");

		} else if (type.equals("s")) {
			Seller user = new Seller();
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmail(email);
			user.setPhoneNumber(phoneNumber);
			user.setCountryOfResidence(countryOfResidence);
			user.setIsActive(isActive);
			user.setIsBanned(isBanned);
			user.setPassword(encryptionServicesLocal.encryptString(password));

			sellerServicesLocal.addSeller(user);
			messagingServicesLocal.sendEmail(email, "WELCOME TO YOU BAY", "WELCOME TO YOU BAY");
		} else {
			Manager user = new Manager();
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmail(email);
			user.setPhoneNumber(phoneNumber);
			user.setCountryOfResidence(countryOfResidence);
			user.setIsActive(isActive);
			user.setIsBanned(isBanned);
			user.setPassword(encryptionServicesLocal.encryptString(password));

			managerServicesLocal.addManager(user);
			messagingServicesLocal.sendEmail(email, "WELCOME TO YOU BAY", "WELCOME TO YOU BAY");
		}

		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correct", "You can log in now !!");
		FacesContext.getCurrentInstance().addMessage(null, msg);

		return navigateTo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCountryOfResidence() {
		return countryOfResidence;
	}

	public void setCountryOfResidence(String countryOfResidence) {
		this.countryOfResidence = countryOfResidence;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsBanned() {
		return isBanned;
	}

	public void setIsBanned(Boolean isBanned) {
		this.isBanned = isBanned;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
