package tn.edu.esprit.sigma.fusion.youbaywar.managedbeans.admin;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SellerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbaywar.managedbeans.utilities.IdentityBean;


@ManagedBean
@ViewScoped
public class ManageSellersBean {
	private Seller seller = new Seller();
	private Seller sellerChosen = new Seller();
	private List<Seller> sellers= new ArrayList<>();
	private List<Product> products= new ArrayList<>();
	private String findWord;
	private Boolean displyTable = false;
	
	@ManagedProperty("#{identityBean}")
	IdentityBean identityBean;
	
	@EJB
	private SellerServicesLocal sellerServicesLocal;

	public String doCreateSeller(){
		sellerServicesLocal.addSeller(sellerChosen);
		displyTable = false;
		return "";
	}
	
	public String doDeleteSeller() {
		sellerServicesLocal.deleteSeller(sellerChosen);
		sellerChosen = new Seller();
		displyTable = false;
		return "";
	}
	
	public void doSelect() {
		displyTable = true;
	}

	public void doDisplayTable() {
		displyTable = true;
	}
	
	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}
	

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<Seller> getSellers() {
		sellers = sellerServicesLocal.findAllSeller();
		return sellers;
	}

	public void setSellers(List<Seller> sellers) {
		this.sellers = sellers;
	}

	public Seller getSellerChosen() {
		return sellerChosen;
	}

	public void setSellerChosen(Seller sellerChosen) {
		this.sellerChosen = sellerChosen;
	}

	public String getFindWord() {
		return findWord;
	}

	public void setFindWord(String findWord) {
		this.findWord = findWord;
	}

	public Boolean getDisplyTable() {
		return displyTable;
	}

	public void setDisplyTable(Boolean displyTable) {
		this.displyTable = displyTable;
	}

	public IdentityBean getIdentityBean() {
		return identityBean;
	}

	public void setIdentityBean(IdentityBean identityBean) {
		this.identityBean = identityBean;
	}
	
	
	
	
}
