package tn.edu.esprit.sigma.fusion.youbaywar.managedbeans.utilities;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Manager;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.YouBayUser;

@ManagedBean
@SessionScoped
public class IdentityBean {
	
	
	public IdentityBean() {
		// TODO Auto-generated constructor stub
	}
	
	private YouBayUser youBayUserfound;

	public YouBayUser getYouBayUserfound() {
		return youBayUserfound;
	}

	public void setYouBayUserfound(YouBayUser youBayUserfound) {
		this.youBayUserfound = youBayUserfound;
	}
	
	
	public Boolean hasRole(String role) {
		Boolean response = false;
		switch (role) {
		case "manager":
			response = youBayUserfound instanceof Manager;
			break;
		case "seller":
			response = youBayUserfound instanceof Seller;
			break;
		case "buyer":
			response = youBayUserfound instanceof Buyer;
			break;
		}
		return response;
	}

}
