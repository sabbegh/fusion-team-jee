package tn.edu.esprit.sigma.fusion.youbay.processing;

import javax.ejb.Remote;

@Remote
public interface PaymentServicesRemote {
public void chargeCreditCard();
}
